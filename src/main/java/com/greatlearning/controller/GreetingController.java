package com.greatlearning.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/greet")
public class GreetingController {

	// adding the request mapping --> URL
	@GetMapping("/")
	public String greet() {
		return "welcome"; // view file jsp file
	}

	// adding the request mapping --> URL
	//by default all RequestMapping is a Get mapping
	@GetMapping("/goodbye")
	public String giveGoodByeMessage() {
		return "goodbye";
	}

}
