package com.greatlearning.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.greatlearning.model.Employee;

@Controller
@RequestMapping("/employees")
public class EmployeeController {
	
	@GetMapping("/register")
	public String showRegistrationForm(Model model) {

		Employee employee = new Employee();

		//it is a map, which accepts key and the value
		// key should be string and should be unique
		//value can be any data type and can be duplicate
		//Model acts as a container to pass the information to the view
		// Model argument is automatically injected by Spring MVC to the method
		model.addAttribute("employee", employee);

		return "employee-form";
	}
	
	// The @ModelAttribute annotation refer to the property of the model object and
		// is used
		// prepare the model data. This annotation bind a method variable or the model
		// object to a
		// named model attribute
		@RequestMapping("/processForm")
		public String processForm(@ModelAttribute("employee") Employee employee) {

			System.out.print(employee.getFirstName());
			return "registration-confirmation";
		}

}
