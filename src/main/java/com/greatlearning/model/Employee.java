package com.greatlearning.model;

import java.time.LocalDate;
import java.util.Arrays;

public class Employee {
	
	
	private String firstName;
	private String lastName;
	private String department;
	private String[] technologies;
	
	
	public Employee() {}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public String[] getTechnologies() {
		return technologies;
	}


	public void setTechnologies(String[] technologies) {
		this.technologies = technologies;
	}


	@Override
	public String toString() {
		return "Employee [firstName=" + firstName + ", lastName=" + lastName + ", department=" + department
				+ ", technologies=" + Arrays.toString(technologies) + "]";
	}
		
}
