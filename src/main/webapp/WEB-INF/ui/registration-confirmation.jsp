<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee confirmation</title>
</head>
<body>

	The registration of employee is confirmed : ${employee.firstName} ${employee.lastName}
	<br>
	<br> The Department assigned is : ${employee.department}
	<br>
	<br> The Technologies that employee knows :
	<!-- we are going to iterate over the object technologies  -->
	<ul>
		<c:forEach var="temp" items="${employee.technologies}">
		
			<li>${temp}</li>
		
		</c:forEach>
	</ul>

</body>
</html>